import System.Environment

main = do
    args <- getArgs
    print (f (read (head args) :: Int))
    where
        f 0 = 0
        f n = n + f (n - 1)
