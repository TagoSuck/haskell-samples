import System.Environment

s :: Int -> Int
s 0 = 0
s n = n + s (n - 1) -- n + (n-1) + ((n-1)-1) ... + 0

main = do
    arg <- getArgs -- get command line arguments
    let len = length arg -- get count of the command line arguments
    if len == 0
    then putStrLn "sample1 NUM" -- when called with no arguments, show hint
    else print (s (read (head arg) :: Int))
