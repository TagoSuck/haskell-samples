import System.Environment

main = do
    args <- getArgs
    let n = read (head args) :: Int
    print ((n `div` 2) * (n + 1))
